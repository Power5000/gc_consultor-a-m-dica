

ALTER PROCEDURE SP_diagnostico(
	@fiebre BIT,
	@dolorDeGarganta BIT,
	@congestion BIT,
	@dolorDeCabeza BIT
)
WITH ENCRYPTION
AS BEGIN
	DECLARE @diagnostico1 varchar(30) = 'Infección de garganta'
	DECLARE @diagnostico2 varchar(30) = 'Resfriado'
	DECLARE @diagnostico3 varchar(30) = 'Alergia'
	IF @dolorDeGarganta = 1
		PRINT 'Usted tiene, ' + @diagnostico1;
	ELSE
		IF @fiebre = 1
			PRINT 'Usted tiene, ' + @diagnostico2;
		ELSE
			IF @dolorDeCabeza = 1
				PRINT 'Usted tiene, ' + @diagnostico2;
			ELSE
				PRINT 'Usted tiene, ' + @diagnostico3;
END
GO

EXECUTE SP_diagnostico 1,1,0,1
EXECUTE SP_diagnostico 0,0,1,0
EXECUTE SP_diagnostico 0,0,1,1
EXECUTE SP_diagnostico 0,0,1,0
EXECUTE SP_diagnostico 1,1,1,1
EXECUTE SP_diagnostico 0,0,1,0
EXECUTE SP_diagnostico 1,0,1,1
EXECUTE SP_diagnostico 1,1,0,1
EXECUTE SP_diagnostico 0,0,1,1
GO