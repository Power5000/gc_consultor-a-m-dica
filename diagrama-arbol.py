fiebre          = 'no'
dolorDeGarganta = 'no'
congestion      = 'no'
dolorDeCabeza   = 'no'
usuario         = ''

reglas = [
    ['Infección de garganta','si','-','-','-'],
    ['Resfriado','no','si','-','-'],
    ['Resfriado','no','no','si'],
    ['Alergia','no','no','no','-']
]

def consulta(): 
    user            = input('Dime ¿Cúal es su nombre? ')
    fiebre          = input('¿Usted padece de fiebre? ')
    dolorDeGarganta = input('¿Usted padece de dolor de Garganta? ')
    congestion      = input('¿Usted padece de congestión? ')
    dolorDeCabeza   = input('¿Usted padece de dolor de Cabeza? ')

    diagnostico([user,dolorDeGarganta,fiebre,dolorDeCabeza,congestion])

def diagnostico(arr): 
    for regla in reglas:
        estado: bool = False

        pos = 0
        for item in regla:
            if pos > 0:
                estado = inferir(estado,arr[pos],item)
            pos+=1
        
        if estado:
            print(arr[0]+', tiene '+regla[0])
            break

def inferir(estado,ask,val): 
    if val == '-': return estado
    else:
        if ask == val: return True
        else: return False

consulta()